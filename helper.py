import requests
from billiard import Pool
from flask import jsonify
import data_parser
from configurations import TIMEOUT
from flask import session


def process_command(data):
	"""
	Make request to the different domain and call corresponding parser.
	"""
	headers = get_request_header(data['request_to'].NAME, data['request_to'].TOKEN)
	url = data['request_to'].URL + data['query']
	s = get_key(data['request_to'].NAME, None)
	try:
		res = {}
		response = s.get(url, headers=headers, timeout=TIMEOUT)
		res = getattr(data_parser, data['request_to'].NAME)(response.json())    #return corresponding parser from data_parser
		set_key(data['request_to'].NAME, s)
		return res
	except requests.exceptions.Timeout:
		res = {}	
		res['message'] = data['request_to'].NAME + " resource not respond within timeframe of "+ str(TIMEOUT) + " seconds."
		return (data['request_to'].NAME, res)


def get_request_header(name, token):
	"""
	Get header according to request made on diff domain.
	"""
	headers = {
		'accept-encoding': "gzip",
    	'user-agent': "my program (gzip)",
    	'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36'
	}
	if name == 'twitter':
		headers['authorization'] = token
	return headers


#function get data and spawn 3 process for each type of request made i.e google, twitter and duckduckgo
def get_search_result(data):
	"""
	Spawn 3 process to for different domain search to work them parallely
	i.e. decrease response of the server.
	"""
	res = {}
	pools = Pool(3)
	results = pools.map(process_command, data)
	pools.close()
	pools.terminate()
	for result in results:
		res[result[0]] = result[1]
	return res


def get_response(data, code):
	response = jsonify(data)
	response.status_code = code
	return response


#Initializing google, twitter, duckduckgo request sessions
def set_request_sessions():
	"""
	Persist certain parameter across request to avoid making new connection again and again
	i.e. decrease response time from server.
	"""
	set_key('google', requests.Session())
	set_key('twitter', requests.Session())
	set_key('duck', requests.Session())
	set_key('all_set', True)


#get session key
def get_key(key, val):
	return session.get(key, val)


#set session key
def set_key(key, val):
	session[key] = val
