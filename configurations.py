#GOOGLE API DETAILS
class GOOGLE:
	TOKEN = ""
	CX = ""
	URL = "https://www.googleapis.com/customsearch/v1?key=" + TOKEN + "&cx=" + CX + "&fields=items(title,link)&num=1&start=0&q="
	NAME = "google"

#DUCK API DETAILS
class DUCK:
	TOKEN = ""
	CX = ""
	URL = "http://api.duckduckgo.com/?format=json&q="
	NAME = "duck"

#TWITTER API DETAILS
class TWITTER:
	TOKEN = ""
	CX = ""
	URL = "https://api.twitter.com/1.1/search/tweets.json?count=1&q="
	NAME = "twitter"


TIMEOUT = 0.6                   #Request timeout time in secnds

SECRETKEY = 'kamehamehaaaa'     #Secret key for sessions


#LOCAL_CONFIGURATION to be imported.
try:
	from local_configurations import *                           
except Exception as e:
	print str(e)
	print "Error importing \'local_configurations.py\'"
	exit(0)
