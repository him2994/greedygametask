from app import app
from flask import request
from datetime import datetime as dt
from helper import get_response, get_search_result, set_request_sessions, get_key, set_key
from configurations import *


REQUEST_TO = [GOOGLE, TWITTER, DUCK]


@app.route('/', methods=['GET'])
def search():
	"""
	Search query over 3 domain(google, duckduckgo, twitter)
	---
	parameters:
		- q: string
		  required: true
	"""
	time = dt.now()
	query = request.args.get('q', None)
	if not query: 
		response = {}
		response['success'] = False
		response['message'] = "Query param \'q\' required. Example ?q=\'your search query\'"
		return get_response(response, 400)
	try:
		if not get_key('all_set', None):
			set_request_sessions()
		response = {}
		res = {}
		response['query'] = query
		data = [{'query':query, 'request_to':x} for x in REQUEST_TO]
		res['success'] = True
		res['data'] = response
		response['results'] = get_search_result(data)
		et = dt.now()-time
		res['time'] = 'server take ' + str(et) + ' time to search results.'
		return get_response(res, 200)
	except Exception as e:
		print e
		response = {}
		response['success'] = False
		response['message'] = "Something happen. Please try again later."
		return get_response(response, 500)
