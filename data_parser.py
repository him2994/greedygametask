#twitter response parser
def twitter(data):
    res = {}
    try:
        status = data['statuses'][0]
        res['text'] = status['text']
        res['url'] = "https://twitter.com/" + status['user']['screen_name'] + "/status/" + status['id_str']
    except:
        res['message'] = 'No result found.'
    return ('twitter', res)

#google response parser
def google(data):
    res = {}
    try:
        status = data['items'][0]
        res['text'] = status['title']
        res['url'] = status['link']
    except:
        res['message'] = 'No result found.'
    return ('google', res)

#ducduckgo response parser
def duck(data):
    res = {}
    try:
        if len(data['Results']):
            status = data['Results'][0]
            res['text'] = data['Heading'] + '-' + status['Text']
            res['url'] = status['FirstURL']
        elif data['Heading'] and data['AbstractURL']:
            res['text'] = data['Heading'] + '-' + data['AbstractSource']
            res['url'] = data['AbstractURL']
        else:
            res['message'] = 'No result found.'         
    except:
        res['message'] = 'No result found.'
    return ('duckduckgo', res)
