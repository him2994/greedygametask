from flask import Flask
from flask_cors import CORS, cross_origin
from flask_session import Session
from configurations import SECRETKEY

app = Flask(__name__)

#app session configurations
app.secret_key = SECRETKEY
app.config['SESSION_TYPE'] = 'filesystem'
sess = Session()
sess.init_app(app)

#app cors configurations
CORS(app)

import router
