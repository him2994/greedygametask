# Greedy Game - task


### Tech

* [Python/Flask] -  Microframework for Python based on Werkzeug
* [Billiard] - Multiprocessing fork for python
* [Requests] - HTTP library for Python
* [Gunicorn] - Pre-fork worker model for python wsgi HTTP server


### Installation

working on python2.7+ or python3.4+

Cloning, installing dependencies and start the server


```sh
$ git clone https://him2994@bitbucket.org/him2994/greedygametask.git
$ cd greedygametask
$ pip install -r requirement.txt
$ python run.py
```

Server is up.
You can access it on this - [http://localhost:5000](http://localhost:5000)
